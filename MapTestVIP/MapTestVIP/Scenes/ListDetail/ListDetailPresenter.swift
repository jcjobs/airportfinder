//
//  ListDetailPresenter.swift
//  MapTestVIP
//
//  Created by Juan Carlos Perez on 22/03/20.
//  Copyright (c) 2020 Juan Carlos Perez. All rights reserved.

import UIKit

protocol ListDetailPresentationLogic
{
    func presentAirportsResult(response: ListDetail.Airports.Response)
}

class ListDetailPresenter: ListDetailPresentationLogic
{
    weak var viewController: ListDetailDisplayLogic?
    func presentAirportsResult(response: ListDetail.Airports.Response) {
        let viewModel = ListDetail.Airports.ViewModel(airportsArray: response.airportsArray)
        viewController?.displayAirports(viewModel: viewModel)
    }
}
