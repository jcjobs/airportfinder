//
//  ListDetailViewController.swift
//  MapTestVIP
//
//  Created by Juan Carlos Perez on 22/03/20.
//  Copyright (c) 2020 Juan Carlos Perez. All rights reserved.


import UIKit

protocol ListDetailDisplayLogic: class
{
    func displayAirports(viewModel: ListDetail.Airports.ViewModel)
}

class ListDetailViewController: UIViewController
{
    @IBOutlet weak var tblAirports : UITableView!
    
    fileprivate var airportsArray = [AirportModel]()
    var interactor: ListDetailBusinessLogic?
    var router: (NSObjectProtocol & ListDetailRoutingLogic & ListDetailDataPassing)?

  // MARK: Object lifecycle
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
  {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder)
  {
    super.init(coder: aDecoder)
    setup()
  }
  
  
  
  // MARK: Routing
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?)
  {
    if let scene = segue.identifier {
      let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
      if let router = router, router.responds(to: selector) {
        router.perform(selector, with: segue)
      }
    }
  }
  
  // MARK: View lifecycle
  
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor?.getAirports()
    }
  
}

private extension ListDetailViewController {
    // MARK: Setup
    
    func setup() {
      let viewController = self
      let interactor = ListDetailInteractor()
      let presenter = ListDetailPresenter()
      let router = ListDetailRouter()
      viewController.interactor = interactor
      viewController.router = router
      interactor.presenter = presenter
      presenter.viewController = viewController
      router.viewController = viewController
      router.dataStore = interactor
    }
    
    func setupUI() {
        tblAirports.delegate = self
        tblAirports.dataSource = self
    }
    
}

extension ListDetailViewController : ListDetailDisplayLogic {
    func displayAirports(viewModel: ListDetail.Airports.ViewModel) {
        airportsArray = viewModel.airportsArray
        tblAirports.reloadData()
    }
}

extension ListDetailViewController : UITableViewDataSource, UITableViewDelegate {
    // MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return airportsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AriportDetailTableViewCellId", for: indexPath) as! AriportDetailTableViewCell
        let currentItem = airportsArray[indexPath.row]
        cell.setupCell(with: currentItem)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }
    
    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
