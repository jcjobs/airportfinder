//
//  ListDetailInteractor.swift
//  MapTestVIP
//
//  Created by Juan Carlos Perez on 22/03/20.
//  Copyright (c) 2020 Juan Carlos Perez. All rights reserved.


import UIKit

protocol ListDetailBusinessLogic
{
    func getAirports()
}

protocol ListDetailDataStore
{
  var airportsArray: [AirportModel] { get set }
}

class ListDetailInteractor: ListDetailBusinessLogic, ListDetailDataStore
{
    var presenter: ListDetailPresentationLogic?
    var airportsArray = [AirportModel]()
    
    func getAirports(){
        let response = ListDetail.Airports.Response(airportsArray: airportsArray)
        presenter?.presentAirportsResult(response: response)
    }
}
