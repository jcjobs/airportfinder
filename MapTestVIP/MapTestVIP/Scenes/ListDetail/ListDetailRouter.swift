//
//  ListDetailRouter.swift
//  MapTestVIP
//
//  Created by Juan Carlos Perez on 22/03/20.
//  Copyright (c) 2020 Juan Carlos Perez. All rights reserved.


import UIKit

@objc protocol ListDetailRoutingLogic
{
  //func routeToSomewhere(segue: UIStoryboardSegue?)
}

protocol ListDetailDataPassing
{
  var dataStore: ListDetailDataStore? { get }
}

class ListDetailRouter: NSObject, ListDetailRoutingLogic, ListDetailDataPassing
{
  weak var viewController: ListDetailViewController?
  var dataStore: ListDetailDataStore?

}
