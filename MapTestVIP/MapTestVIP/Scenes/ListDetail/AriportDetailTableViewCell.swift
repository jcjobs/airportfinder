//
//  AriportDetailTableViewCell.swift
//  MapTestVIP
//
//  Created by Juan Carlos Perez on 22/03/20.
//  Copyright © 2020 Juan Carlos Perez. All rights reserved.
//

import UIKit

class AriportDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblCity : UILabel!
    @IBOutlet weak var lblLatitude : UILabel!
    @IBOutlet weak var lblLongitude : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setupCell(with airport: AirportModel){
        lblName.text = airport.name
        lblCity.text = airport.city
        if let location = airport.location {
            lblLatitude.text = "\(location.latitude)"
            lblLongitude.text = "\(location.longitude)"
        }
        
    }

}
