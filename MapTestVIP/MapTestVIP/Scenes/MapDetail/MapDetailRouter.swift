//
//  MapDetailRouter.swift
//  MapTestVIP
//
//  Created by Juan Carlos Perez on 22/03/20.
//  Copyright (c) 2020 Juan Carlos Perez. All rights reserved.


import UIKit

@objc protocol MapDetailRoutingLogic
{
  //func routeToSomewhere(segue: UIStoryboardSegue?)
}

protocol MapDetailDataPassing
{
  var dataStore: MapDetailDataStore? { get }
}

class MapDetailRouter: NSObject, MapDetailRoutingLogic, MapDetailDataPassing
{
  weak var viewController: MapDetailViewController?
  var dataStore: MapDetailDataStore?

}
