//
//  MapDetailInteractor.swift
//  MapTestVIP
//
//  Created by Juan Carlos Perez on 22/03/20.
//  Copyright (c) 2020 Juan Carlos Perez. All rights reserved.

import UIKit

protocol MapDetailBusinessLogic
{
    func getAirports()
}

protocol MapDetailDataStore
{
  var airportsArray: [AirportModel] { get set }
}

class MapDetailInteractor: MapDetailBusinessLogic, MapDetailDataStore
{
  var presenter: MapDetailPresentationLogic?
  var airportsArray = [AirportModel]()
    
    func getAirports() {
        let response = MapDetail.Airports.Response(airportsArray: airportsArray)
        presenter?.presentAirportsResult(response: response)
    }
    
}
