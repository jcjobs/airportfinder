//
//  MapDetailViewController.swift
//  MapTestVIP
//
//  Created by Juan Carlos Perez on 22/03/20.
//  Copyright (c) 2020 Juan Carlos Perez. All rights reserved.


import UIKit
import MapKit
import CoreLocation

protocol MapDetailDisplayLogic: class
{
    func displayAirports(viewModel: MapDetail.Airports.ViewModel)
}

class MapDetailViewController: UIViewController{
    
    @IBOutlet weak var mapViewResult: MKMapView!
    
    var manager = CLLocationManager()
    var latitud : CLLocationDegrees!
    var longitud : CLLocationDegrees!
    
    var interactor: MapDetailBusinessLogic?
    var router: (NSObjectProtocol & MapDetailRoutingLogic & MapDetailDataPassing)?

  // MARK: Object lifecycle
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
  {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder)
  {
    super.init(coder: aDecoder)
    setup()
  }
  
  // MARK: Routing
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?)
  {
    if let scene = segue.identifier {
      let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
      if let router = router, router.responds(to: selector) {
        router.perform(selector, with: segue)
      }
    }
  }
  
  // MARK: View lifecycle
  
  override func viewDidLoad()
  {
    super.viewDidLoad()
    
    
    mapViewResult.delegate = self
     manager.delegate = self
     manager.requestWhenInUseAuthorization()
     manager.desiredAccuracy = kCLLocationAccuracyBest
     //manager.startUpdatingLocation()
     manager.requestLocation()
    
     
     // Do any additional setup after loading the view.
    

     //let localizacion = CLLocationCoordinate2DMake(19.4284706, -99.1276627)
     //let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
     //let region = MKCoordinateRegion(center: localizacion, span: span)
     //mapViewResult.setRegion(region, animated: true)
     mapViewResult.showsUserLocation = true
     
  }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        clearMap()
        interactor?.getAirports()
    }
    
}

private extension MapDetailViewController {
    
    // MARK: Setup
    func setup() {
       let viewController = self
       let interactor = MapDetailInteractor()
       let presenter = MapDetailPresenter()
       let router = MapDetailRouter()
       viewController.interactor = interactor
       viewController.router = router
       interactor.presenter = presenter
       presenter.viewController = viewController
       router.viewController = viewController
       router.dataStore = interactor
    }
    
    func clearMap(){
        self.mapViewResult.annotations.forEach {
            if !($0 is MKUserLocation) {
               self.mapViewResult.removeAnnotation($0)
            }
        }
    }
    
    func setPin(item: AirportModel){
        let annotation = MKPointAnnotation()
        annotation.title = item.name
        annotation.subtitle = item.city
        annotation.coordinate = CLLocationCoordinate2D(latitude: item.location?.latitude ?? 0.0 , longitude: item.location?.longitude ?? 0.0)
        mapViewResult.addAnnotation(annotation)
    }
    
    func showAlertMessage(with message: String){
        let alert = UIAlertController(title: "¡Aviso!", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension MapDetailViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "annotationView")
        annotationView.canShowCallout = true
        annotationView.rightCalloutAccessoryView = UIButton.init(type: .detailDisclosure)

        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let titel = view.annotation?.title as? String {
            print(titel)
        }
    }
}

extension MapDetailViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            self.latitud = location.coordinate.latitude
            self.longitud = location.coordinate.longitude
            
            manager.stopUpdatingLocation()
            
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 5.0, longitudeDelta: 5.0))
            self.mapViewResult.setRegion(region, animated: true)
        }
    }

    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            manager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: (error)")
    }
}


extension MapDetailViewController: MapDetailDisplayLogic {
    func displayAirports(viewModel: MapDetail.Airports.ViewModel) {
        for airport in viewModel.airportsArray {
            setPin(item: airport)
        }
    }
}
