//
//  MapDetailPresenter.swift
//  MapTestVIP
//
//  Created by Juan Carlos Perez on 22/03/20.
//  Copyright (c) 2020 Juan Carlos Perez. All rights reserved.

import UIKit

protocol MapDetailPresentationLogic
{
     func presentAirportsResult(response: MapDetail.Airports.Response)
}

class MapDetailPresenter: MapDetailPresentationLogic
{
  weak var viewController: MapDetailDisplayLogic?
    func presentAirportsResult(response: MapDetail.Airports.Response) {
        let viewModel = MapDetail.Airports.ViewModel(airportsArray: response.airportsArray)
        viewController?.displayAirports(viewModel: viewModel)
    }
}
