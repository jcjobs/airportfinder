//
//  MapDetailModels.swift
//  MapTestVIP
//
//  Created by Juan Carlos Perez on 22/03/20.
//  Copyright (c) 2020 Juan Carlos Perez. All rights reserved.

import UIKit

enum MapDetail
{
    
    enum Airports
    {
      struct Request
      {
      }
      struct Response
      {
        let airportsArray: [AirportModel]
      }
      struct ViewModel
      {
        let airportsArray: [AirportModel]
      }
    }
}
