//
//  SearchRedioPresenter.swift
//  MapTestVIP
//
//  Created by Juan Carlos Perez on 22/03/20.
//  Copyright (c) 2020 Juan Carlos Perez. All rights reserved.

import UIKit

protocol SearchRedioPresentationLogic
{
    func presentResults(response: SearchRedio.DefineRedio.Response)
    func presentError(response: SearchRedio.DefineRedio.Response)
}

class SearchRedioPresenter: SearchRedioPresentationLogic
{
    weak var viewController: SearchRedioDisplayLogic?
    
    func presentResults(response: SearchRedio.DefineRedio.Response){
        
        if response.airportsResult.count > 0 {
            let viewModel = SearchRedio.DefineRedio.ViewModel()
            viewController?.displayResults(viewModel: viewModel)
        } else {
            viewController?.displayEmptyResults()
        }
        
    }
    
    func presentError(response: SearchRedio.DefineRedio.Response) {
        viewController?.displayErrorMessage(with: response.error?.localizedDescription ?? "Unknown error")
    }
    
}
