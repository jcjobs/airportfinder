//
//  SearchRedioWorker.swift
//  MapTestVIP
//
//  Created by Juan Carlos Perez on 22/03/20.
//  Copyright (c) 2020 Juan Carlos Perez. All rights reserved.

import UIKit

class SearchRedioWorker
{
    
    func searchAirportsInRadius(with radius:Int, latitude: Double, longitude: Double, success: @escaping ([AirportModel]) -> (), failure: @escaping (NSError) -> ()) {
        
        let params : [String: Any] = ["radius":"\(radius)", "lng":"\(longitude)" ,"lat": "\(latitude)"]
        let serviceConnector = ServiceConnector()
        serviceConnector.makeConnectionWithParametters(function:  "by-radius", params: params, requestType: .get) { (result) in
            
            switch result{
            case .success(let httpCode, let httResponseDesc, let responseData):
                print("Response: " + httResponseDesc + " With code: \(httpCode)" )
                
                guard let data = responseData else{
                    failure( NSError.init(domain: httResponseDesc, code: httpCode, userInfo: [NSLocalizedDescriptionKey: "No data"]) )
                    return
                }
                
                let parsingResult = self.parseAriportsData(data: data)
                if let errorParsing = parsingResult.1 {
                     failure(errorParsing)
                } else {
                    success(parsingResult.0)
                }
                break
            case .failure(let error):
                failure(error)
                break
            }
        }
        
    }
}

private extension SearchRedioWorker {

    func parseAriportsData(data dataResponse: Data) -> ([AirportModel], NSError?) {
        var currentAirports = [AirportModel]()
        
        do {
            let jsonResponse = try JSONSerialization.jsonObject(with: dataResponse, options: []) as! [[String: AnyObject]]

            for item in jsonResponse{
               var airport = AirportModel()
               airport.airportId = item["airportId"] as? String ?? ""
               airport.code = item["code"] as? String ?? ""
               airport.name = item["name"] as? String ?? ""
                                 
               if let location  = item["location"] as? [String:Any] {
                   let lng = location["longitude"] as? Double ?? 0.0
                   let lat = location["latitude"] as? Double ?? 0.0
                   airport.location = AirpotlocationModel(longitude: lng, latitude: lat)
               }

               airport.cityId = item["cityId"] as? String ?? ""
               airport.city = item["city"] as? String ?? ""
               airport.countryCode = item["countryCode"] as? String ?? ""
                             
               currentAirports.append(airport)
            }
        } catch let error as NSError {
            //print("error trying to convert data to JSON")
            return (currentAirports, error)
        }
        
        
        return (currentAirports, nil)
    }
    
}
