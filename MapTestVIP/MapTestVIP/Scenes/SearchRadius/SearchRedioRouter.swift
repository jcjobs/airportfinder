//
//  SearchRedioRouter.swift
//  MapTestVIP
//
//  Created by Juan Carlos Perez on 22/03/20.
//  Copyright (c) 2020 Juan Carlos Perez. All rights reserved.

import UIKit

@objc protocol SearchRedioRoutingLogic
{
  func routeToResults(segue: UIStoryboardSegue?)
}

protocol SearchRedioDataPassing
{
  var dataStore: SearchRedioDataStore? { get }
}

class SearchRedioRouter: NSObject, SearchRedioRoutingLogic, SearchRedioDataPassing
{
  weak var viewController: SearchRedioViewController?
  var dataStore: SearchRedioDataStore?
  
  // MARK: Routing
    
    func routeToResults(segue: UIStoryboardSegue?) {
      if let segue = segue {
        let destinationVC = segue.destination as! SearchResultTabBarController
        var destinationDS = destinationVC.router!.dataStore!
        passDataToResults(source: dataStore!, destination: &destinationDS)
      } else {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destinationVC = storyboard.instantiateViewController(withIdentifier: "SearchResultTabBarControllerId") as! SearchResultTabBarController
        var destinationDS = destinationVC.router!.dataStore!
        passDataToResults(source: dataStore!, destination: &destinationDS)
        navigateToResults(source: viewController!, destination: destinationVC)
      }
    }
    

    // MARK: Navigation
    func navigateToResults(source: SearchRedioViewController, destination: SearchResultTabBarController) {
        source.show(destination, sender: nil)
    }
    
    // MARK: Passing data
    func passDataToResults(source: SearchRedioDataStore, destination: inout SearchResultDataStore) {
        destination.airportsArray = source.airportsArray
    }
    
}
