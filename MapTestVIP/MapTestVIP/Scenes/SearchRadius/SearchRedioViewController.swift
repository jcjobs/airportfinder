//
//  SearchRedioViewController.swift
//  MapTestVIP
//
//  Created by Juan Carlos Perez on 22/03/20.
//  Copyright (c) 2020 Juan Carlos Perez. All rights reserved.


import UIKit

import CoreLocation
import MapKit

protocol SearchRedioDisplayLogic: class
{
    func displayResults(viewModel: SearchRedio.DefineRedio.ViewModel)
    func displayEmptyResults()
    func displayErrorMessage(with errorMessage: String)
}

class SearchRedioViewController: UIViewController
{
    
    @IBOutlet weak var btnSearch : UIButton!
    @IBOutlet weak var activityIndicator : UIActivityIndicatorView!
    @IBOutlet weak var sldRadius : UISlider!
    @IBOutlet weak var lblRadiusValue : UILabel!
    
    var locationManager = CLLocationManager()
    fileprivate var currentUserLocation :CLLocation?
    
    var interactor: SearchRedioBusinessLogic?
    var router: (NSObjectProtocol & SearchRedioRoutingLogic & SearchRedioDataPassing)?

    
  // MARK: Object lifecycle
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  // MARK: Routing
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?)
  {
    if let scene = segue.identifier {
      let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
      if let router = router, router.responds(to: selector) {
        router.perform(selector, with: segue)
      }
    }
  }
  
  // MARK: View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupUI()
    requestLocation()
    setupNavigationBar()
  }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        locationManager.startUpdatingLocation()
    }
  
    
    @IBAction func search(_ sender: UIButton) {
        sender.isEnabled = false
        activityIndicator.startAnimating()
        let radius = calculateRadius()
        let lat = currentUserLocation?.coordinate.latitude ?? 0.0
        let lng = currentUserLocation?.coordinate.longitude ?? 0.0
        let request = SearchRedio.DefineRedio.Request(radius: radius, latitude: lat, longitude: lng)
        interactor?.requestAirports(request: request)
    }
    
    @IBAction func sliderValueChanged(_ sender: Any) {
        let currentValue = calculateRadius()
        lblRadiusValue.text = "\(currentValue)"
    }
    
}

private extension SearchRedioViewController {
    
    // MARK: Setup
    func setup() {
        let viewController = self
        let interactor = SearchRedioInteractor()
        let presenter = SearchRedioPresenter()
        let router = SearchRedioRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    func setupUI() {
        activityIndicator.hidesWhenStopped = true
        
        btnSearch.isExclusiveTouch = true
        btnSearch.layer.cornerRadius = 10
        btnSearch.layer.borderWidth = 1
        btnSearch.layer.borderColor = UIColor.clear.cgColor
        
        let currentValue = calculateRadius()
        lblRadiusValue.text = "\(currentValue)"
    }
    
    func setupNavigationBar(){
        self.navigationItem.title = "Search"
    }
    
    func calculateRadius() -> Int{
        return Int(sldRadius.value * 1000)
    }
    
    func requestLocation() {
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation() //requestLocation()
        }
    }
    
    func showAlertMessage(with message: String){
        let alert = UIAlertController(title: "¡Aviso!", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension SearchRedioViewController : SearchRedioDisplayLogic {
       
    func displayResults(viewModel: SearchRedio.DefineRedio.ViewModel) {
        btnSearch.isEnabled = true
        activityIndicator.stopAnimating()
        router?.routeToResults(segue: nil)
    }
    
    func displayEmptyResults() {
        btnSearch.isEnabled = true
        activityIndicator.stopAnimating()
        showAlertMessage(with: "No hubo datos de la consulta")
    }
    
    func displayErrorMessage(with errorMessage: String) {
        btnSearch.isEnabled = true
        activityIndicator.stopAnimating()
        showAlertMessage(with:errorMessage)
    }
}

extension SearchRedioViewController : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        btnSearch.isEnabled = true
        
        if let location = locations.last {
            manager.stopUpdatingLocation()
            currentUserLocation = location
        }
        //let userLocation: CLLocation = locations.last! as CLLocation
        //currentUserLocation = userLocation
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            manager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
        //xshowAlertMessage(with:error.localizedDescription)
    }
}
