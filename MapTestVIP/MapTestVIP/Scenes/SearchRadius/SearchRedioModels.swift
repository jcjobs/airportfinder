//
//  SearchRedioModels.swift
//  MapTestVIP
//
//  Created by Juan Carlos Perez on 22/03/20.
//  Copyright (c) 2020 Juan Carlos Perez. All rights reserved.

import UIKit

enum SearchRedio
{
  // MARK: Use cases
    
    enum DefineRedio
    {
      struct Request
      {
        let radius: Int
        let latitude: Double
        let longitude: Double
      }
      struct Response
      {
        let airportsResult:[AirportModel]
        let error: NSError?
      }
      struct ViewModel
      {
      }
    }
    
}
