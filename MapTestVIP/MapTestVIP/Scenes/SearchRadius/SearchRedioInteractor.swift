//
//  SearchRedioInteractor.swift
//  MapTestVIP
//
//  Created by Juan Carlos Perez on 22/03/20.
//  Copyright (c) 2020 Juan Carlos Perez. All rights reserved.

import UIKit

protocol SearchRedioBusinessLogic
{
    func requestAirports(request: SearchRedio.DefineRedio.Request)
}

protocol SearchRedioDataStore
{
    var airportsArray: [AirportModel] { get set }
}

class SearchRedioInteractor: SearchRedioBusinessLogic, SearchRedioDataStore
{
    
    var presenter: SearchRedioPresentationLogic?
    var worker: SearchRedioWorker?

    var airportsArray = [AirportModel]()
  
    
    func requestAirports(request: SearchRedio.DefineRedio.Request) {
        worker = SearchRedioWorker()
        
        self.airportsArray.removeAll()
        worker?.searchAirportsInRadius(with: request.radius, latitude: request.latitude, longitude: request.longitude, success: { (airportsResult) in
            self.airportsArray = airportsResult
            let response = SearchRedio.DefineRedio.Response(airportsResult: self.airportsArray, error: nil)
            self.presenter?.presentResults(response: response)
        }, failure: { (failureReason) in
             let response = SearchRedio.DefineRedio.Response(airportsResult: self.airportsArray, error: nil)
             self.presenter?.presentError(response: response)
        })
    }
}
