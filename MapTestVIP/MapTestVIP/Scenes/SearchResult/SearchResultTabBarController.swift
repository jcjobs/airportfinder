//
//  SearchResultViewController.swift
//  MapTestVIP
//
//  Created by Juan Carlos Perez on 22/03/20.
//  Copyright (c) 2020 Juan Carlos Perez. All rights reserved.


import UIKit

protocol SearchResultDisplayLogic: class
{
  
}

class SearchResultTabBarController: UITabBarController
{
  var router: (NSObjectProtocol & SearchResultRoutingLogic & SearchResultDataPassing)?

  // MARK: Object lifecycle
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
  {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder)
  {
    super.init(coder: aDecoder)
    setup()
  }
  
  // MARK: Setup
  
  private func setup()
  {
    let viewController = self
    let interactor = SearchResultInteractor()
    let router = SearchResultRouter()
    viewController.router = router
    router.viewController = viewController
    router.dataStore = interactor
  }
  
  // MARK: Routing
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?)
  {
    if let scene = segue.identifier {
      let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
      if let router = router, router.responds(to: selector) {
        router.perform(selector, with: segue)
      }
    }
  }
  
  // MARK: View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    delegate = self
    goToMapView()
  }
  
}

private extension SearchResultTabBarController {
    func goToMapView(){
        setupNavigationBarForMap()
        if let viewC = self.viewControllers?.first {
            router?.routeToMapResults(itemVC: viewC)
        }
    }
    
    func setupNavigationBarForMap(){
        self.navigationItem.title = "Map View Results"
    }
    
    func setupNavigationBarForList(){
        self.navigationItem.title = "List View Results"
    }
}

extension SearchResultTabBarController : SearchResultDisplayLogic{

}

extension SearchResultTabBarController : UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if viewController.isKind(of: MapDetailViewController.self) {
            setupNavigationBarForMap()
            router?.routeToMapResults(itemVC: viewController)
        } else if viewController.isKind(of: ListDetailViewController.self) {
            setupNavigationBarForList()
            router?.routeToListResults(itemVC: viewController)
        }
        return false
    }
}

