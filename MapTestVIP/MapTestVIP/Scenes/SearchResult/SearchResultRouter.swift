//
//  SearchResultRouter.swift
//  MapTestVIP
//
//  Created by Juan Carlos Perez on 22/03/20.
//  Copyright (c) 2020 Juan Carlos Perez. All rights reserved.


import UIKit

@objc protocol SearchResultRoutingLogic
{
    func routeToMapResults(itemVC: UIViewController)
    func routeToListResults(itemVC: UIViewController)
}

protocol SearchResultDataPassing
{
  var dataStore: SearchResultDataStore? { get }
}

class SearchResultRouter: NSObject, SearchResultRoutingLogic, SearchResultDataPassing
{
  weak var viewController: SearchResultTabBarController?
  var dataStore: SearchResultDataStore?
  
    // MARK: Routing
   func routeToMapResults(itemVC: UIViewController) {
        let destinationVC = itemVC as! MapDetailViewController
        var destinationDS = destinationVC.router!.dataStore!
        passDataToMapView(source: dataStore!, destination: &destinationDS)
        navigateToMapView(source: viewController!, destination: destinationVC)
    }
    
    func routeToListResults(itemVC: UIViewController) {
        let destinationVC = itemVC as! ListDetailViewController
        var destinationDS = destinationVC.router!.dataStore!
        passDataToListView(source: dataStore!, destination: &destinationDS)
        navigateToListView(source: viewController!, destination: destinationVC)
    }

    // MARK: Navigation
    func navigateToMapView(source: SearchResultTabBarController, destination: MapDetailViewController) {
        source.selectedIndex = 0
    }
    
    func navigateToListView(source: SearchResultTabBarController, destination: ListDetailViewController) {
        source.selectedIndex = 1
    }
     
    // MARK: Passing data
    func passDataToMapView(source: SearchResultDataStore, destination: inout MapDetailDataStore) {
        destination.airportsArray = source.airportsArray ?? [AirportModel]()
    }
    
    func passDataToListView(source: SearchResultDataStore, destination: inout ListDetailDataStore) {
            destination.airportsArray = source.airportsArray ?? [AirportModel]()
    }
  
}
