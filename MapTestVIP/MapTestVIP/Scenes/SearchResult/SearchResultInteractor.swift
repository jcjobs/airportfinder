//
//  SearchResultInteractor.swift
//  MapTestVIP
//
//  Created by Juan Carlos Perez on 22/03/20.
//  Copyright (c) 2020 Juan Carlos Perez. All rights reserved.


import UIKit

protocol SearchResultDataStore
{
    var airportsArray: [AirportModel]? { get set }
}

class SearchResultInteractor: SearchResultDataStore
{
    var airportsArray: [AirportModel]?
}
