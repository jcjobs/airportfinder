//
//  ServiceErrorParser.swift
//  MapTestVIP
//
//  Created by Juan Carlos Perez on 22/03/20.
//  Copyright © 2020 Juan Carlos Perez. All rights reserved.
//

import UIKit

struct CGenericResponse: Codable{
   
    var message:  String {
            return meta.message ?? ""
          }
  
    var statusCode:  Int {
              return meta.statusCode ?? 0
            }
    
   private struct Meta : Codable {
          let statusCode: Int?
          let message: String?
   }
    
    private let meta: Meta
    
}

class ServiceErrorParser: NSObject {
    
    class func makeParseWith(data dataResponse: Data) -> NSError? {
           
           do {
            let json = try JSONSerialization.jsonObject(with: dataResponse, options: []) as! [String: AnyObject]
               
                let errorMessage = json["message"] as? String ?? ""
                let errorStatus = json["status"] as? Int ?? 0

                return NSError.init(domain: errorMessage, code: errorStatus, userInfo:[NSLocalizedDescriptionKey: errorMessage])
               
           } catch {//let error as NSError {
                return nil
           }
           
           
       }

}
