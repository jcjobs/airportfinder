//
//  ServiceConnector.swift
//  MapTestVIP
//
//  Created by Juan Carlos Perez on 22/03/20.
//  Copyright © 2020 Juan Carlos Perez. All rights reserved.
//

import UIKit

public enum Environment {
    case Develompment
    case Production
}

public var environmentType : Environment = .Develompment
public var SESSION_URL_SERVER: String {
    get {
        switch environmentType {
        case .Develompment:
            return  "https://cometari-airportsfinder-v1.p.rapidapi.com"//Url Dev
        case .Production:
            return  "https://cometari-airportsfinder-v1.p.rapidapi.com" //Url Prod
        }
    }
}

public var SESSION_WS_EXTENSION: String {
    get {
        switch environmentType {
        case .Develompment:
            return  "/api/airports/" //Url Dev
        case .Production:
            return  "/api/airports/" //Url Prod
        }
    }
}

let APP_SESSION_TIMEOUT = 10.0//Tiempo en minutos antes de finalizar la sesión por inactividad
let TIMED_OUT_INTERVAL = 120.0//Tiempo en segundos antes de finalizar la petición al host remoto

class ServiceConnector: NSObject {
        
        //TODO:Handle error with NetworkError enum
        enum NetworkError: Swift.Error {
            case badURL
            case requestFailed(reason:String, code:Int)
            case unknown
        }
        
        enum RequestType {
            case post
            case get
            case put
            case delete
            case none
        }
        
        func getQueryStringParameter(_ url: String, param: String) -> String? {
            guard let url = URLComponents(string: url) else { return nil }
            return url.queryItems?.first(where: { $0.name == param })?.value
        }
    
        
        func makeConnectionWithParametters(function: String, params: Dictionary<String, Any>, requestType: RequestType, completion:@escaping(Result<(Int, String, Data?), NSError>) ->() ) {
            
            var httResponseCode = 0
            var responseDescription = ""
            var responseData : Data?
            
            let urlString = SESSION_URL_SERVER.appending(SESSION_WS_EXTENSION)
            
            //Verificar que se cuenta con acceso a internet
            guard ReachabilityManager.isConnectedToNetwork() else {
                DispatchQueue.main.async(execute: {
                    responseDescription = "No dispones de una conexión estable a internet, intenta más tarde."
                    completion(.failure(NSError.init(domain: responseDescription, code: 503, userInfo: [NSLocalizedDescriptionKey: responseDescription])))
                })
                return
            }
            
            guard var urlComp = URLComponents(string: urlString.appending(function)) else {
                responseDescription = "URL inválida."
                let userInfo: [String : Any]  = [
                    NSLocalizedDescriptionKey : #file + "-" + #function + "-\(#line)",
                    NSLocalizedFailureReasonErrorKey : responseDescription
                ]
                completion(.failure(NSError.init(domain: responseDescription, code: 500, userInfo: userInfo)))
                return
            }
            
            var theRequest = URLRequest(url: urlComp.url!)
            theRequest.timeoutInterval = TIMED_OUT_INTERVAL
            
            switch requestType {
            case .post:
                theRequest.httpMethod = "POST"
                guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
                    return
                }
                theRequest.httpBody = httpBody
                break
            case .get:
                theRequest.httpMethod = "GET"
                urlComp.queryItems = params.map { (arg) -> URLQueryItem in
                    let (key, value) = arg
                    return URLQueryItem(name: key, value: value as? String)
                }
                                   
                theRequest = URLRequest(url: urlComp.url!)
                break
            default:
                break
            }
            
            theRequest.setValue("cometari-airportsfinder-v1.p.rapidapi.com", forHTTPHeaderField: "X-RapidAPI-Host")
            theRequest.setValue("96979b1369msh5f69aa976887443p19ea37jsn3cb326642fd3", forHTTPHeaderField: "X-RapidAPI-Key")
            
    
            print("<SERVICE CONNECTOR> - Service: \(theRequest.description)")
            print("<SERVICE CONNECTOR> - Request: \(convertToJson(dictionary: params))")
            print("<SERVICE CONNECTOR> - Request headers: " + String(describing: theRequest.allHTTPHeaderFields) )
            
            
            let session = URLSession.shared
            let task = session.dataTask(with: theRequest as URLRequest, completionHandler: { data, response, error in
                
                var errorResult : NSError?
                
                if let httpResponse  = response as! HTTPURLResponse? {
                    
                    if let dataResponse = data{
                        print("<<<<<Request data: " + (String(data: dataResponse, encoding: .utf8) ?? "No Data.") )
                    }

                    httResponseCode = httpResponse.statusCode
                    switch(httpResponse.statusCode) {
                    case 200..<300:
                        print("Response success with status code: [\(httpResponse.statusCode)]")
                        
                        responseDescription = "Response success with status code: [\(httpResponse.statusCode)]"
                        responseData = data
                        DispatchQueue.main.async(execute: {
                            completion(.success( (httResponseCode, responseDescription, responseData) ))
                        })
                        
                        break
                    case 400..<500:
                        print("Request failed: \(httpResponse.description)")
                        responseDescription =  "Error en la petición."
                        
                        if let dataResponse = data{
                            errorResult = ServiceErrorParser.makeParseWith(data: dataResponse)
                        }
                        
                        if errorResult == nil{
                            errorResult = NSError.init(domain: responseDescription, code: httpResponse.statusCode, userInfo: [NSLocalizedDescriptionKey: responseDescription])
                        }
                       
                        DispatchQueue.main.async(execute: {
                            completion(.failure(errorResult!))
                        })
                        
                        break
                    case 500..<600:
                        print("Server error: \(httpResponse.description)")
                        responseDescription =  "Error de comunicaciones."
                        
                        if let dataResponse = data{
                             errorResult = ServiceErrorParser.makeParseWith(data: dataResponse)
                        }
                         
                        if errorResult == nil{
                             errorResult = NSError.init(domain: responseDescription, code: httpResponse.statusCode, userInfo: [NSLocalizedDescriptionKey: responseDescription])
                        }
                        
                        DispatchQueue.main.async(execute: {
                            completion(.failure(errorResult!))
                        })
                        
                        break
                    default:
                        print("Request failed: \(httpResponse.description)")
                        responseDescription =  "Error de comunicaciones."
                        
                        if let dataResponse = data{
                             errorResult = ServiceErrorParser.makeParseWith(data: dataResponse)
                        }
                         
                        if errorResult == nil{
                             errorResult = NSError.init(domain: responseDescription, code: httpResponse.statusCode, userInfo: [NSLocalizedDescriptionKey: responseDescription])
                        }
                        
                        DispatchQueue.main.async(execute: {
                            completion(.failure(errorResult!))
                        })
                        break
                    }
                }
                
                if let errorDetail  = error as NSError? {
                    print("Connection failed with error:\(error!.localizedDescription)" )
                    
                    //-1001 Timed Out | -1005 network connection lost
                    if(httResponseCode == -1001 || httResponseCode == -1005) {
                        responseDescription = "Error de comunicaciones, intente nuevamente."
                    }
                    else if (httResponseCode == 710 || httResponseCode == 418 ){
                        responseDescription = "Error de comunicaciones, intente nuevamente."
                    }
                    
                    DispatchQueue.main.async(execute: {
                        completion(.failure(errorDetail))
                    })
                }
                
            })
            task.resume()
        }
        
        
        private func convertToJson(dictionary: [String: Any]) -> String {
            var result = ""
            
            if let dataJSON = try? JSONSerialization.data(
                withJSONObject: dictionary,
                options: .prettyPrinted),
                let jsonText = String(
                    data: dataJSON,
                    encoding: .ascii) {
                
                result = jsonText
            }
            
            return result
        }
        
    }

