//
//  ReachabilityManager.swift
//  MapTestVIP
//
//  Created by Juan Carlos Perez on 22/03/20.
//  Copyright © 2020 Juan Carlos Perez. All rights reserved.
//

import Foundation
import SystemConfiguration

class ReachabilityManager: NSObject {
  
     // MARK: - Obtener la dirección IP
   
   /// Devuelve la dirección IP de la interfaz WiFi (en0) como String o `nil`
   ///
   /// - Returns: Regresa la dirección IP
   class func isConnectedToNetwork() -> Bool {
       var zeroAddress = sockaddr_in()
       zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
       zeroAddress.sin_family = sa_family_t(AF_INET)
       
       let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
           $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
               SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
           }
       }
       
       var flags = SCNetworkReachabilityFlags()
       if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
           return false
       }
       let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
       let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
       return (isReachable && !needsConnection)
       
   }
    
}
