//
//  AirpotModel.swift
//  MapTestVIP
//
//  Created by Juan Carlos Perez on 22/03/20.
//  Copyright © 2020 Juan Carlos Perez. All rights reserved.
//

import UIKit

struct AirportModel {
    var airportId = ""
    var code = ""
    var name = ""
    var location: AirpotlocationModel?
    var cityId = ""
    var city = ""
    var countryCode = ""
}

struct AirpotlocationModel {
    var longitude: Double
    var latitude: Double
}


/*
"airportId": "5d5448a4-f7d7-461d-8ec0-7881719cc013",
       "code": "HNL",
       "name": "Honolulu, Oahu",
       "location": {
           "longitude": -157.925,
           "latitude": 21.321667
       },
       "cityId": "7b244a1b-b2fb-4e9b-a611-3f3107f9adfc",
       "city": "Honolulu",
       "countryCode": "US",
*/
